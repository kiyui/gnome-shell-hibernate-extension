# gnome-shell-hibernate-extension

A GNOME extension that adds the option to hibernate amongst other system actions

![A screenshot of the GNOME power submenu with an additional “Hibernate” action under the “Suspend” action](./images/screenshot.png)

## installation

- Available as a ZIP on the [releases page](https://codeberg.org/kiyui/gnome-shell-hibernate-extension/releases)
- Available from the GNOME extensions website [here](https://extensions.gnome.org/extension/3814/system-action-hibernate/)

## tips

If you enjoy this extension, consider buying me a coffee 😄

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/D1D34NIY5)

## contributors

- [@KopfKrieg](https://codeberg.org/KopfKrieg) [#5](https://codeberg.org/kiyui/gnome-shell-hibernate-extension/pulls/5)
- [@nhermosilla14](https://codeberg.org/nhermosilla14) [#7](https://codeberg.org/kiyui/gnome-shell-hibernate-extension/pulls/7)
- [@keyile](https://codeberg.org/keyile) [#9](https://codeberg.org/kiyui/gnome-shell-hibernate-extension/pulls/9)
- [@pervoj-codeberg](https://codeberg.org/pervoj-codeberg) [#10](https://codeberg.org/kiyui/gnome-shell-hibernate-extension/pulls/10)
- [@SlyDeath](https://codeberg.org/SlyDeath) [#11](https://codeberg.org/kiyui/gnome-shell-hibernate-extension/pulls/11)
- [@ViBE](https://codeberg.org/ViBE) [#12](https://codeberg.org/kiyui/gnome-shell-hibernate-extension/pulls/12)
